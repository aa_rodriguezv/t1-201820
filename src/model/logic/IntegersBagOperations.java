package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}
			
		}
		return min;
	}
	
	public String invertSigns(IntegersBag bag)
	{
		String numeros = "";
		int value = 0;
		
		
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				value = value*(-1);
				numeros += value + " ";
			}
			
		}
		return numeros;
	}
	
	public ArrayList<Integer> arrayListOfOpposites(IntegersBag bag)
	{
		String numeros = this.invertSigns(bag);
		String[] minArray = numeros.split(" ");
		ArrayList<Integer> arregloNumeros = new ArrayList<Integer>();
		for(int i=0; i<minArray.length; i++)
		{
			arregloNumeros.add(Integer.parseInt(minArray[i]));
		}
		
		return arregloNumeros;
	}
	
	public int getSum(IntegersBag bag)
	{
		int sum = 0;
		int value = 0;
		
		if(bag!= null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				sum += value;
			}
		}
		
		return sum;
	}
}



